import { extendTheme } from "@chakra-ui/react";
import { ButtonStyles as Button } from "./components/buttonStyles";
import { mode } from "@chakra-ui/theme-tools";
import { TypographyStyles as Typography } from "./components/typographyStyles";

export const myTheme = extendTheme({
  colors: {
    primary: "#5E25BC",
    secondary: "#D6EBF6",
    highlight: "#CD88FF",
    danger: "#EE321C",
  },
  components: {
    Button,
    Typography,
  },
  config: {
    initialColorMode: "light",
    useSystemColorMode: false,
  },
  styles: {
    global: (props) => ({
      body: {
        color: mode("#4B4453", "#FEF7FF")(props),
        bg: mode("white", "#141214")(props),
      },
    }),
  },
});
