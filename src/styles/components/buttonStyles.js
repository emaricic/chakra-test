import { whiten, darken, mode } from "@chakra-ui/theme-tools";

export const ButtonStyles = {
  // Styles for the base style
  baseStyle: { borderRadius: "20px", fontWeight: "bold" },
  // Styles for the size variations
  sizes: {
    lg: { width: "200px", height: "50px", borderRadius: "10px" },
  },
  // Styles for the visual style variations
  variants: {
    primary: (props) => ({
      bg: "#8B74A5",
      color: mode("black", "white")(props),
      _hover: {
        bg: mode(whiten("primary", 20), darken("primary", 20))(props),
        boxShadow: "md",
      },
    }),
    secondary: (props) => ({
      bg: "secondary",
      color: mode("black", "black")(props),

      _hover: {
        bg: mode(whiten("secondary", 20), darken("secondary", 20))(props),
        boxShadow: "md",
        transform: "scale(1.02)",
      },
      "@media screen and (max-width: 900px)": {
        color: "red",
        fontSize: 10,
      },
    }),
    danger: (props) => ({
      bg: "danger",
      color: mode("black", "black")(props),

      _hover: {
        bg: mode(whiten("danger", 20), darken("danger", 10))(props),

        transform: "scale(1.02)",
      },
      "@media screen and (max-width: 900px)": {
        color: "red",
        fontSize: 10,
      },
    }),
  },
  // The default `size` or `variant` values
  defaultProps: {},
};
