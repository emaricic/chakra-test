import {
  Box,
  Button,
  Center,
  Heading,
  Text,
  useColorMode,
  useMediaQuery,
} from "@chakra-ui/react";

function App() {
  const { colorMode, toggleColorMode } = useColorMode();
  const [isLargerThan1280] = useMediaQuery("(min-width: 1280px)");

  return (
    <Box>
      <Center borderColor="red" h="80vh" maxW="1200px" mx="auto" gap={8}>
        <Box p={20}>
          <Heading variant="large" fontSize={30} color="red">
            I'm a Heading
          </Heading>
          <Text variant="textPrimary">
            {colorMode === "light"
              ? "Hello Light"
              : "Hello darkness my old friend"}
          </Text>
          <Text variant="textSecondary">
            {colorMode === "light"
              ? "Testing text 2"
              : "Testing text 2 dark mode"}
          </Text>
          <Text>
            {isLargerThan1280 ? "larger than 1280px" : "smaller than 1280px"}
          </Text>
        </Box>
        <Button onClick={toggleColorMode}>
          Toggle {colorMode === "light" ? "Dark" : "Light"}
        </Button>
        <Button>Clean button</Button>
        <Button variant="primary" size="lg">
          Primary
        </Button>
        <Button variant="secondary">Secondary</Button>
        <Button variant="danger">Danger</Button>
      </Center>
    </Box>
  );
}

export default App;
